# Custom debian ISO

- [Custom debian ISO](#custom-debian-iso)
  - [Objectif](#objectif)
    - [Requirements](#requirements)
    - [Our strategies guidelines](#our-strategies-guidelines)
  - [Usage](#usage)
    - [Install dependencies](#install-dependencies)
    - [Create a config file](#create-a-config-file)
    - [Build the custom ISO](#build-the-custom-iso)
    - [Install with the ISO](#install-with-the-iso)
    - [Connect to the host with ssh](#connect-to-the-host-with-ssh)
    - [Make some clean](#make-some-clean)
  - [Advanced informations](#advanced-informations)
    - [Structure on the ISO](#structure-on-the-iso)
  - [For developpement](#for-developpement)
    - [TODO LIST](#todo-list)


## Objectif

Make a **custom debian 12 Bookworm ISO** (or usb key) **full automated** with **minimal external dependencies** (ex: software, network, ...).

### Requirements

- a Linux OS like Ubuntu to use this code.
- Internet access to download the **ISO** and install some tools.

### Our strategies guidelines

In order of importance:
- Security
- Trusted input
  - We start by using Official Debian 12 iso
  - This code source
- Minimal external dependencies:
  - Minimal sofware interaction
  - Minimal network usage during installation. (we using CD not Netinstall iso)
- Easy to use
  - ISO usable for with USB key install
  - ISO usable with hypervisor like virtualbox
- Easy to understand
  - Minimal code usage (simple, small bash scripts)
  - Well explain and understandable(this also part of the security)
- Easy to modify
  - We use small, simple bash script
- Make the code more of less indempotent, i.e. do nothing if already done

## Usage

### Install dependencies

| Step | Command | Description | Target directory |
|-|-|-|-|
| 1 | `./bin/1_init.sh`                | Install 7z and xorriso needed to open and make iso | (see description) |

### Create a config file

```bash
# Create custom config file
cp config.origin config
```

Now, edit your the file `./config` and custom variables.
We strongly recomment to change passwords.

### Build the custom ISO

| Step | Command | Description | Target directory |
|-|-|-|-|
| 2.1 | `./bin/2.1_download.sh`          | Download the Debian 12 iso and check the hash                 | `input/`    |
| 2.2 | `./bin/2.2_extract_iso.sh`       | Extract the iso                                               | `isofiles/` |
| 2.3 | `./bin/2.3_update_grub.sh`       | Update grub menu to add default entry to use preseed          | `isofiles/` |
| 2.4 | `./bin/2.4_generate_preseed_and_keys.sh`| Generate ssh key server and preseed.cfg using template | `generate/` |
| 2.5 | `./bin/2.5_add_preseed_and_keys` | Add preseed and server key in iso files                       | `isofiles/` |
| 2.6 | `./bin/2.6_make_iso.sh`          | Generate custum iso                                           | `ouput/`    |
||
| 2.all | `./bin/2.all.sh`               | Exec all 2.x scripts one by one                                | (see description) |

### Install with the ISO

Now, you have the iso in `output/`.

You can load it on hypervisor like Virtualbox or copy in USB to make installation on physical server.

To make USB key bootable, do
```bash
# Identify your USB key device (Ex: /dev/sdd) 
ls /dev/sd*
#or
lsblk
#or
dmesg

# Format your key
sudo umount /media
gparted
# Create partion MSDOS
# Format in fat32

# Copy the iso on USB key
sudo dd if=output/debian-12.2.0-amd64-DVD-1-custom.iso of=/dev/CHANTEIT bs=1M status=progress iflag=direct
sync
```
More information on https://www.cyberciti.biz/faq/creating-a-bootable-ubuntu-usb-stick-on-a-debian-linux/

### Connect to the host with ssh

| Step | Command | Description | Target directory |
|-|-|-|-|
| 3.1 | `./bin/3.1_gerenrate_client_config.sh` | Generate ssh_config and known_host (Strict mode for security)| `ssh/`      |
| 3.2 | `./bin/3.2_connect.sh`           | Connect to the vm by ssh    |


The step 3.2 can be make manualy with:
```bash
HOSTNAME="debian12"  # Change it with you host name
ssh -F ./ssh/${HOSTNAME}.ssh_config ${HOSTNAME}
```

### Make some clean

| Step | Command | Description | Target directory |
|-|-|-|-|
| 4.1 | `./bin/4.1_clean.sh`             | Remove generate file                                          | `isofiles/`, `generate/`, `output/` |
| 4.2 | `./bin/4.2_clean_all.sh`         | Remove all (including Debian 12 Iso and ssh client config)    | `input/` , `isofiles/`, `generate/`, `ouput/`, `ssh/` |



## Advanced informations

### Structure on the ISO

Main folders to focus on would be boot, casper and isolinux.

- `boot` folder holds on the installer options of live system that is used for installation.
- `casper` holds in compresses filesystem called `squashfs`` files as well as INITial Ram Disk (init-rd) file for loading filesystem and vmlinuz file which is essential Linux kernel.
- `isolinux` which provides configuration files for boot system among other things.

Custom Debian, CD
- https://wiki.debian.org/DebianInstaller/Modify/CD


## For developpement

### TODO LIST

- [x] Test by remove preseed of initrd
- [x] Specify networking
  - [x] For installation
  - [x] Post installation (already user by the previus)
- [x] Auto reboot after installation
- [x] Add authorized_keys:
  - [x] debian user
- [x]Add Sudoer password less
- [x] block ssh 
  - [x] root all: use `echo "PermitRootLogin no" > /etc/ssh/sshd_config.d/custom` 
  - [x] debian only ssh key
    - test: `ssh -o PubkeyAuthentication=no -o PreferredAuthentications=password`
- [x] Change debian user name (use configuration file)
- [x] Specify ssh server keys
  - [x] ssh_host_ecdsa_key: `ssh-keygen -t ecdsa -b 256 -f ssh_host_ecdsa_key -q -N "" -C "root@debian12"`
  - [x] ssh_host_ed25519_key: `ssh-keygen -t ed25519 -f ssh_host_ed25519_key -q -N "" -C "root@debian12"`
  - [x] ssh_host_rsa_key: `ssh-keygen -t rsa -b 3072 -f ssh_host_rsa_key -q -N "" -C "root@debian12"`
- [x] Template
  - [x] Generate know_hosts
  - [x] Generate ssh_config
  - [x] Generate unique ssh key
- [x] Centrelize with generale config bash file
- [x] LVM partion
  - [x] Keep free space on disk (use trick: add partition and delete it with late command)
- [x] Write documentation
- [x] Publish
