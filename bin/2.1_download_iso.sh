#!/bin/bash
cd "$(dirname "$0")/.."

# Variables

# Web site: https://cdimage.debian.org/debian-cd/current/amd64/iso-dvd/
ISO_URL=https://cdimage.debian.org/debian-cd/current/amd64/iso-dvd/debian-12.2.0-amd64-DVD-1.iso
ISO_FILE=debian-12.2.0-amd64-DVD-1.iso
ISO_HASH="2e0f5ddcccb3f02c2b53882acfa4fed88c2e4a73bfb5605f949d5c99ea1975b9d8782411ff1fc7d791bfd8ef0b7edefcd1b43e42468fcf8cc9a967a68d6b41f2"

# Web site: https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/
#ISO_URL=https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-12.2.0-amd64-netinst.iso
#ISO_FILE=debian-12.2.0-amd64-netinst.iso
#ISO_HASH="11d733d626d1c7d3b20cfcccc516caff2cbc57c81769d56434aab958d4d9b3af59106bc0796252aeefede8353e2582378e08c65e35a36769d5cf673c5444f80e"


# Web: https://www.proxmox.com/en/downloads/proxmox-virtual-environment/iso


if [ ! -d "./input" ]; then
  mkdir ./input
fi

cd ./input
if [ ! -f ${ISO_FILE} ]; then
  wget ${ISO_URL}
else
  echo "$ISO_FILE already exist"
fi

if [[ ! $(sha512sum $ISO_FILE) = "$ISO_HASH  $ISO_FILE" ]]; then
    echo "Checksum failed" >&2
    exit 1
else
  echo "Checksum valid"
fi

cd ..

echo "DONE"