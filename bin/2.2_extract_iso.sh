#!/bin/bash
cd "$(dirname "$0")/.."

# Variables
ISO_FILE=debian-12.2.0-amd64-DVD-1.iso
#ISO_FILE=debian-12.2.0-amd64-netinst.iso

if [ ! -d "isofiles" ]; then
    mkdir isofiles

    #bsdtar -C isofiles -xf ./input/${ISO_FILE}
    # or
    #xorriso -osirrox on -indev ./input/${ISO_FILE} -extract / isofiles
    # or
    7z x -oisofiles ./input/${ISO_FILE}
    # or
    #udevil mount ./input/${ISO_FILE}
    # cp -rT /media/${ISO_FILE}/ isofiles/
fi

#mv ./iso/\[BOOT\] ./boot

echo "DONE"