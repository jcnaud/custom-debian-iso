#!/bin/bash
cd "$(dirname "$0")/.."

ISO_DIR="./isofiles"

# Update menu
cd ${ISO_DIR}
chmod u+w isolinux
#sed -i 's/^default.*$/default auto/m' isolinux/isolinux.cfg
#sed -i 's/^prompt.*$/prompt 1/m' isolinux/isolinux.cfg
sed -i 's/^timeout.*$/timeout 100/m' isolinux/isolinux.cfg
chmod u-w isolinux


# ATTENTION, This is tab, not spaces
chmod u+w isolinux/txt.cfg
cat <<EOF > isolinux/txt.cfg
label install
	menu label ^Install
	kernel /install.amd/vmlinuz
	append vga=788 initrd=/install.amd/initrd.gz --- quiet 
label netinstall
	menu label ^Install auto via preseed
	menu default
	kernel /install.amd/vmlinuz
	append auto=true vga=normal file=/cdrom/preseed.cfg initrd=/install.amd/initrd.gz locale=fr_FR.UTF-8 console-keymaps-at/keymap=fr-latin9
timeout 100
ontimeout /install.amd/vmlinuz auto=true vga=normal file=/cdrom/preseed.cfg initrd=/install.amd/initrd.gz locale=fr_FR.UTF-8 console-keymaps-at/keymap=fr-latin9
menu autoboot Press a key, otherwise install auto via preseed will be started in # second{,s}...
EOF
chmod u-w isolinux/txt.cfg

# Disable auto speech menu entry
cat <<EOF > isolinux/spkgtk.cfg
label installspk
	menu label Install with ^speech synthesis
	kernel /install.amd/vmlinuz
	append vga=788 initrd=/install.amd/gtk/initrd.gz speakup.synth=soft --- quiet 
# timeout to speech-enabled install
#timeout 300
#ontimeout /install.amd/vmlinuz vga=788 initrd=/install.amd/gtk/initrd.gz speakup.synth=soft --- quiet 
#menu autoboot Press a key, otherwise speech synthesis will be started in # second{,s}...
EOF

cd ..

echo "DONE"
