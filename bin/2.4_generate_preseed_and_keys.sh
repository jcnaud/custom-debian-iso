#!/bin/bash
cd "$(dirname "$0")/.."

# Load Config file
. ./config

#######################################################################
echo "Generate SSH keys for server: ./generate"
#######################################################################
if [ ! -d ./generate ]; then
  mkdir ./generate
fi
cd ./generate

# Generate ssh server key to pregenerate known_hosts
if [ ! -f "ssh_host_ecdsa_key" ]; then
  ssh-keygen -t ecdsa -b 256 -f ssh_host_ecdsa_key -q -N "" -C "root@${HOSTNAME}"
fi

if [ ! -f "ssh_host_ed25519_key" ]; then
  ssh-keygen -t ed25519 -f ssh_host_ed25519_key -q -N "" -C "root@${HOSTNAME}"
fi

if [ ! -f "ssh_host_rsa_key" ]; then
  ssh-keygen -t rsa -b 3072 -f ssh_host_rsa_key -q -N "" -C "root@${HOSTNAME}"
fi
cd ..

#######################################################################
echo "Generate preseed.config: ./generate"
#######################################################################
cp ./data/preseed.template.cfg ./generate/preseed.cfg 
sed -i "s|{{HOSTNAME}}|${HOSTNAME}|g" ./generate/preseed.cfg
sed -i "s|{{ADDRESS}}|${ADDRESS}|g"   ./generate/preseed.cfg
sed -i "s|{{NETMASK}}|${NETMASK}|g"   ./generate/preseed.cfg
sed -i "s|{{GATEWAY}}|${GATEWAY}|g"   ./generate/preseed.cfg
sed -i "s|{{DNS}}|${DNS}|g"           ./generate/preseed.cfg
sed -i "s|{{DOMAIN}}|${DOMAIN}|g"     ./generate/preseed.cfg
sed -i "s|{{ROOT_PASSWORD}}|${ROOT_PASSWORD}|g" ./generate/preseed.cfg
sed -i "s|{{USER}}|${USER}|g" ./generate/preseed.cfg
sed -i "s|{{USER}}|${USER}|g" ./generate/preseed.cfg
sed -i "s|{{USER_PASSWORD}}|${USER_PASSWORD}|g" ./generate/preseed.cfg
sed -i "s|{{DISK_SIZE_MIN}}|${DISK_SIZE_MIN}|g" ./generate/preseed.cfg
sed -i "s|{{DISK_SIZE_MAX}}|${DISK_SIZE_MAX}|g" ./generate/preseed.cfg
sed -i "s|{{SSH_PORT}}|${SSH_PORT}|g" ./generate/preseed.cfg 
AUTHORIZED_KEY=$(cat ${USER_AUTHORIZED_KEY})
sed -i "s|{{AUTHORIZED_KEY}}|${AUTHORIZED_KEY}|g" ./generate/preseed.cfg




#SSH_KEY_ED25519=$(cat ./generate/ssh_host_ed25519_key)
#echo $SSH_KEY_ED25519
#sed -i "s|{{SSH_KEY_ED25519}}|${SSH_KEY_ED25519}|g" ./generate/preseed.cfg 
## TODO add server keys



# Add preseed inside initr.gz
#ISO_DIR=isofiles
#chmod +w -R ${ISO_DIR}/install.amd/
#gunzip ${ISO_DIR}/install.amd/initrd.gz
#cpio -idv < ${ISO_DIR}/install.amd/initrd > file

#echo ./data/preseed.cfg | cpio -H newc -o -A -F ${ISO_DIR}/install.amd/initrd
#gzip ${ISO_DIR}/install.amd/initrd
#chmod -w -R ${ISO_DIR}/install.amd/


#bsdtar -C isofiles -xf ./input/${ISO_FILE}
# or
#xorriso -osirrox on -indev ./input/${ISO_FILE} -extract / isofiles
# or
###7z x -oisofiles ./input/${ISO_FILE}
# or
#udevil mount ./input/${ISO_FILE}
# cp -rT /media/${ISO_FILE}/ isofiles/



#mv ./iso/\[BOOT\] ./boot

echo "DONE"