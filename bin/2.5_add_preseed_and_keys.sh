#!/bin/bash
cd "$(dirname "$0")/.."

ISO_DIR="./isofiles"

# Add preseed inside initr.gz
#chmod +w -R ${ISO_DIR}/install.amd/
#gunzip ${ISO_DIR}/install.amd/initrd.gz
#echo ./data/preseed.cfg | cpio -H newc -o -A -F ${ISO_DIR}/install.amd/initrd
#gzip ${ISO_DIR}/install.amd/initrd
#chmod -w -R ${ISO_DIR}/install.amd/

echo "Add preseed.cfg file"
# If presed already exist 
chmod u+w ${ISO_DIR}
if [ -f ${ISO_DIR}/preseed.cfg ]; then
  chmod u+w ${ISO_DIR}/preseed.cfg
fi

# put new preseed
cp ./generate/preseed.cfg $ISO_DIR/preseed.cfg 
chmod a-w ${ISO_DIR}/preseed.cfg
chmod u-w ${ISO_DIR}


echo "Add ssh keys server"
chmod u+w ${ISO_DIR}
mkdir -p ${ISO_DIR}/preseed.d/
chmod u+w ${ISO_DIR}/preseed.d/

files=("ssh_host_ecdsa_key" "ssh_host_ecdsa_key.pub" "ssh_host_ed25519_key" "ssh_host_ed25519_key.pub" "ssh_host_rsa_key" "ssh_host_rsa_key.pub")
for file in ${files[@]}; do
  if [ ! -f  "${ISO_DIR}/preseed.d/$file" ]; then
    echo "Copy $file"
    cp ./generate/$file ${ISO_DIR}/preseed.d/$file
  fi
done
chmod a-w ${ISO_DIR}/preseed.d/
chmod u-w ${ISO_DIR}
echo "DONE"


# Regenerate md5
echo "Regenerate md5 files"
cd ${ISO_DIR}
chmod +w md5sum.txt
find -follow -type f ! -name md5sum.txt -print0 | xargs -0 md5sum > md5sum.txt
chmod -w md5sum.txt
cd ..