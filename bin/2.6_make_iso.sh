#!/bin/bash
cd "$(dirname "$0")/.."

ISO_FILE=debian-12.2.0-amd64-DVD-1-custom.iso
#ISO_OUTPUT=debian-12.2.0-amd64-netinst-custom.iso

# Help to get current flag iso
# xorriso -indev ./input/debian-12.2.0-amd64-DVD-1.iso -report_el_torito as_mkisofs

#md5sum iso/.disk/info > iso/md5sum.txt
#sed -i 's|iso/|./|g' iso/md5sum.txt
#xorriso -as mkisofs \
#  -r -V "PVE Custom" \
#  -o proxmox-ve_8.1-1-custom.iso -J -l -b boot/memtest86+x64.bin \
#  -c boot/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -eltorito-alt-boot -e boot/grub/efi.img -no-emul-boot -isohybrid-gpt-basdat -isohybrid-apm-hfsplus -isohybrid-mbr /usr/lib/syslinux/bios/isohdpfx.bin iso/boot iso

mkdir -p ./output

xorriso -as mkisofs -o ./output/${ISO_FILE}  \
    -isohybrid-mbr /usr/lib/ISOLINUX/isohdpfx.bin \
    -c isolinux/boot.cat -b isolinux/isolinux.bin -no-emul-boot \
    -boot-load-size 4 -boot-info-table isofiles
            