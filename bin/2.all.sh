#!/bin/bash
cd "$(dirname "$0")"

./2.1_download_iso.sh
./2.2_extract_iso.sh
./2.3_update_grub.sh
./2.4_generate_preseed_and_keys.sh
./2.5_add_preseed_and_keys.sh
./2.6_make_iso.sh