#!/bin/bash
cd "$(dirname "$0")/.."

# Load Config file
. ./config

#######################################################################
echo "Generate SSH config in  ./ssh"
#######################################################################*
mkdir -p ./ssh
cp ./data/ssh_config.template ./ssh/${HOSTNAME}.ssh_config

sed -i "s|{{HOSTNAME}}|${HOSTNAME}|g" ./ssh/${HOSTNAME}.ssh_config
sed -i "s|{{ADDRESS}}|${ADDRESS}|g" ./ssh/${HOSTNAME}.ssh_config
sed -i "s|{{USER}}|${USER}|g" ./ssh/${HOSTNAME}.ssh_config
sed -i "s|{{SSH_PORT}}|${SSH_PORT}|g" ./ssh/${HOSTNAME}.ssh_config
sed -i "s|{{CLIENT_SSH_KEY}}|${CLIENT_SSH_KEY}|g" ./ssh/${HOSTNAME}.ssh_config
sed -i "s|{{CLIENT_KNOWN_HOSTS}}|${CLIENT_KNOWN_HOSTS}|g" ./ssh/${HOSTNAME}.ssh_config


#######################################################################
echo "Generate SSH known_hosts: ./ssh"
#######################################################################
echo "" > ./ssh/${HOSTNAME}.known_hosts

KEY_ECDSA_PUB=`cat ./generate/ssh_host_ecdsa_key.pub`
KEY_ED25519_PUB=`cat ./generate/ssh_host_ed25519_key.pub`
KEY_RSA_PUB=`cat ./generate/ssh_host_rsa_key.pub`
echo "[${HOSTNAME}]:${SSH_PORT} ${KEY_ECDSA_PUB}"   >  ./ssh/${HOSTNAME}.known_hosts
echo "[${ADDRESS}]:${SSH_PORT} ${KEY_ECDSA_PUB}"    >> ./ssh/${HOSTNAME}.known_hosts
echo "[${HOSTNAME}]:${SSH_PORT} ${KEY_ED25519_PUB}" >> ./ssh/${HOSTNAME}.known_hosts
echo "[${ADDRESS}]:${SSH_PORT} ${KEY_ED25519_PUB}"  >> ./ssh/${HOSTNAME}.known_hosts
echo "[${HOSTNAME}]:${SSH_PORT} ${KEY_RSA_PUB}"     >> ./ssh/${HOSTNAME}.known_hosts
echo "[${ADDRESS}]:${SSH_PORT} ${KEY_RSA_PUB}"      >> ./ssh/${HOSTNAME}.known_hosts

