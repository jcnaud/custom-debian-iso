#!/bin/bash
cd "$(dirname "$0")/.."

# Load Config file
. ./config

# Connect
ssh -F ./ssh/${HOSTNAME}.ssh_config ${HOSTNAME}