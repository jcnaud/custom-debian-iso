#!/bin/bash
cd "$(dirname "$0")/.."

files=("./isofiles" "./generate" "./output")

for file in ${files[@]}; do
  if [ -d $file ]; then
    echo "Delete $file"
    chmod -R u+w $file
    rm -rf $file
  fi
done
